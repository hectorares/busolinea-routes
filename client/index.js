'use strict';

import './style';

const apiUrl = 'https://0sysjslkra.execute-api.us-east-1.amazonaws.com/test';
const datepicker = require('vanilla-datepicker');

datepicker();
var makeRequest = function(url, method, data) {
  var request = new XMLHttpRequest();

  return new Promise(function(resolve, reject) {
    request.onreadystatechange = function() {
      if (request.readyState !== 4) return;

      if (request.status >= 200 && request.status < 300) {
        resolve(JSON.parse(request.response));
      } else {
        reject({
          status: request.status,
          statusText: request.statusText
        });
      }
    };

    const body = data ? data : '';
    console.log('body:', body);
    request.open(method || 'GET', url, true);
    request.setRequestHeader(
      'x-api-key',
      'Yrk3AZ1yOT7PIBbWJNrkB541cLBnff5w6cSZH9qr'
    );
    request.setRequestHeader('Accept', 'application/json');
    request.setRequestHeader('Content-Type', 'application/json');

    request.send(JSON.stringify(body));
  });
};

const loadRoutes = function() {
  makeRequest(`${apiUrl}/routes`, 'GET')
    .then(function(posts) {
      const routes = posts.data.routes;
      console.log('routes!', routes);
      reload();

      routes.forEach(route => {
        var listItem = createNewTaskElement(route);

        //Append listItem to incompleteTasksHolder
        incompleteTasksHolder.appendChild(listItem);
        bindTaskEvents(listItem);
      });
    })
    .catch(function(error) {
      console.log('Something went wrong', error);
    });
};

var loadStations = function() {
  makeRequest(`${apiUrl}/stations/origins`, 'GET')
    .then(function(posts) {
      const stations = posts.data.stations;
      console.log('stations!', stations);

      let option;
      stations.forEach(station => {
        option = document.createElement('option');
        option.text = station.name;
        option.value = station.name;
        dropdownOrigin.add(option);
      });

      let optionDestination;
      stations.forEach(station => {
        optionDestination = document.createElement('option');
        optionDestination.text = station.name;
        optionDestination.value = station.name;
        dropdownDestination.add(optionDestination);
      });
    })
    .catch(function(error) {
      console.log('Something went wrong', error);
    });
};

loadRoutes();

var companyInput = document.getElementById('company');
var dropdownOrigin = document.getElementById('origin-dropdown');
var dropdownDestination = document.getElementById('destination-dropdown');

var addButton = document.getElementsByTagName('button')[0]; //first button
var incompleteTasksHolder = document.getElementById('incomplete-tasks'); //incomplete-tasks

var initDate = document.getElementById('initDate');
var endDate = document.getElementById('endDate');
loadStations();

var createNewTaskElement = function(route) {
  //Create List Item
  var listItem = document.createElement('tr');

  var company = document.createElement('td');
  var origin = document.createElement('td');
  var destination = document.createElement('td');
  var departureTime = document.createElement('td');
  var arrivalTime = document.createElement('td');

  //button.delete
  var deleteButton = document.createElement('button');

  deleteButton.innerText = 'Eliminar';
  deleteButton.className = 'delete';
  deleteButton.id = route.id;

  company.innerText = route.company_name;
  origin.innerText = route.origin_id;
  destination.innerText = route.destination_id;
  departureTime.innerText = route.departure_time;
  arrivalTime.innerText = route.arrival_time;

  listItem.appendChild(company);
  listItem.appendChild(origin);
  listItem.appendChild(destination);
  listItem.appendChild(departureTime);
  listItem.appendChild(arrivalTime);
  listItem.appendChild(deleteButton);

  return listItem;
};
const randomNumber = length => {
  return Math.floor(Math.random() * length);
};

const generateId = length => {
  const possible = '0123456789';
  let text = '';

  for (let i = 0; i < length; i++) {
    text += possible.charAt(randomNumber(possible.length));
  }

  return text;
};

function reload() {
  document.getElementById('incomplete-tasks').innerHTML = null;
  console.log('Refreshed');
}

const addRoute = function() {
  console.log('companyInput:', companyInput.value);
  console.log('dropdownOrigin:', dropdownOrigin.value);
  console.log('dropdownDestination:', dropdownDestination.value);
  console.log('id:', generateId(5));
  console.log('initDate:', initDate.value);
  console.log('endDate:', endDate.value);

  const request = {
    id: generateId(5),
    origin_id: dropdownOrigin.value,
    destination_id: dropdownDestination.value,
    departure_time: initDate.value,
    arrival_time: endDate.value,
    company_name: companyInput.value
  };

  makeRequest(`${apiUrl}/routes`, 'POST', request)
    .then(function(res) {
      console.log('res:', res);
      loadRoutes();
      companyInput.value = '';
    })
    .catch(function(error) {
      console.log('Something went wrong', error);
    });
};

const deleteRoute = function(id) {
  makeRequest(`${apiUrl}/routes/${id}`, 'DELETE')
    .then(function(res) {
      console.log('res:', res);
      loadRoutes();
    })
    .catch(function(error) {
      console.log('Something went wrong', error);
      showMessage(
        'Ocurrió un error al eliminar esta ruta, pongase en contacto con el administrador.'
      );
    });
};

var bindTaskEvents = function(taskListItem) {
  console.log('Bind list item events');

  //Select taskListItem's children
  var deleteButton = taskListItem.querySelector('button.delete');
};

var ajaxRequest = function() {
  console.log('AJAX request');
};

const showMessage = function(message) {
  alert(message);
};

//Set the click handler to the addRoute function
addButton.onclick = addRoute;
addButton.addEventListener('click', addRoute);
addButton.addEventListener('click', ajaxRequest);

addButton.onclick = ajaxRequest;

document.addEventListener(
  'click',
  function(event) {
    if (event.target.classList.contains('delete')) {
      console.log('delete------------->', JSON.stringify(event.target.id));
      const id = event.target.id;
      deleteRoute(id);
    }
  },
  false
);
